import shutil
from pathlib import Path

import pytest

# from requests.exceptions import ConnectionError as conn_err

pytest_plugins = ("fw_gear_testing",)


@pytest.fixture(scope="function")
def clear_dir():
    def clear_dir_fun(path):
        shutil.rmtree(str(Path(path).absolute()))

    return clear_dir_fun


# @pytest.fixture(scope="module", autouse=True)
# def download_test_docker_image():
#    client = docker.from_env()
#    client.images.pull("alpine:latest")


# def pytest_runtest_setup(item):
#    for mark in item.iter_markers():
#        if mark.name == "docker":
#            # Marked as needing docker, skip this test if docker not needed.
#            try:
#                client = docker.APIClient(base_url="unix://var/run/docker.sock")
#            except ConnectionError:
#                try:
#                    client = docker.APIClient(base_url="tcp://docker:2375")
#                except ConnectionError:
#                    pytest.skip()
#                except docker.errors.DockerException:
#                    pytest.skip()
#            except docker.errors.DockerException:
#                try:
#                    client = docker.APIClient(base_url="tcp://docker:2375")
#                except ConnectionError:
#                    pytest.skip()
#                except docker.errors.DockerException:
#                    pytest.skip()
#        elif mark.name == "minicore":
#            # Marked as needing minicore
#            import requests
#
#            try:
#                resp = requests.get("http://localhost:8080/api/config")
#                if resp.status_code != 200:
#                    pytest.skip()
#            except ConnectionRefusedError:
#                pytest.skip()
#            except conn_err:
#                pytest.skip()
#
#        else:
#            continue
#
#
def pytest_configure(config):
    config.addinivalue_line(
        "markers", "docker: Marked test needs a docker daemon to run"
    )


def pytest_addoption(parser):
    parser.addoption(
        "--docker", action="store_true", help="Run tests with docker mark."
    )


def pytest_runtest_setup(item):
    try:
        next(item.iter_markers(name="docker"))
        need_docker = True
    except StopIteration:
        need_docker = False
    have_docker = item.config.getoption("--docker")
    if need_docker and not have_docker:
        pytest.skip("Run with `--docker` flag if we have docker")
