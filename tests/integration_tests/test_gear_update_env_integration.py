import argparse
import json
import logging
from pathlib import Path

import docker
import pytest
from flywheel_gear_toolkit.utils.manifest import Manifest

from flywheel_gear_cli.gear.gear_update_env import gear_update_env


@pytest.fixture(scope="session", autouse=True)
def pull_test_image():
    client = docker.from_env()
    client.images.pull("ubuntu:latest")


@pytest.fixture
def manifest_vals():
    """Dummy manifest with environment

    Returns:
        [type]: [description]
    """
    return {
        "name": "ubuntu",
        "version": "latest",
        "environment": {
            "DEBIAN_FRONTEND": "noninteractive",
            "FLYWHEEL": "/flywheel/v0",
            "FSLDIR": "/usr/share/fsl/5.0",
            "PATH": (
                "/usr/share/fsl/5.0/bin:/usr/local/sbin:"
                "/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:"
            ),
            "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0:/usr/share/fsl/5.0/lib",
            "FSLOUTPUTTYPE": "NIFTI_GZ",
            "FSLTCLSH": "/usr/bin/tclsh",
            "FSLWISH": "/usr/bin/wish",
        },
    }


@pytest.mark.docker
def test_gear_update_env_dry_run(mocker, caplog, tmpdir, manifest_vals):
    mocker.patch.object(Manifest, "validate")

    temp = Path(tmpdir).resolve()
    manifest_file = Path(temp / "manifest.json")
    with open(manifest_file, "w") as fp:
        json.dump(manifest_vals, fp)

    args = argparse.Namespace(dir=str(temp), dry_run=True, blocklist=["HOME=no"])

    gear_update_env(args)

    with open(manifest_file, "r") as fp:
        new_conf = json.load(fp)
    assert new_conf == manifest_vals  # Nothing changed

    for record in caplog.record_tuples:
        assert record[1] in [logging.INFO, logging.DEBUG]


@pytest.mark.docker
def test_gear_update_env_wet_run(mocker, caplog, tmpdir, manifest_vals):
    caplog.set_level(logging.INFO)
    mocker.patch.object(Manifest, "validate")

    temp = Path(tmpdir).resolve()
    manifest_file = Path(temp / "manifest.json")
    with open(manifest_file, "w") as fp:
        json.dump(manifest_vals, fp)

    args = argparse.Namespace(dir=str(temp), blocklist=["HOME=no"])

    gear_update_env(args)

    with open(manifest_file, "r") as fp:
        new_conf = json.load(fp)

    env = new_conf["environment"]
    assert "HOME" in env
