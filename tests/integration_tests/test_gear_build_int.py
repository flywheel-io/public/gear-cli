import argparse
import json
import logging
from pathlib import Path

import pytest

from flywheel_gear_cli.gear.gear_build import gear_build


@pytest.mark.docker
@pytest.mark.parametrize("args", [argparse.Namespace(rm=True)])
def test_gear_build(args, caplog, tmpdir, manifest, run, dockerfile):

    workdir = Path(tmpdir)
    args.dir = workdir
    with open(str((workdir / "manifest.json").resolve()), "w") as fp:
        fp.write(json.dumps(manifest))
    with open(str((workdir / "run.py").resolve()), "w") as fp:
        fp.write(run)
    with open(str((workdir / "Dockerfile").resolve()), "w") as fp:
        fp.write(dockerfile)
    _id = gear_build(args)

    im_name = "flywheel/test:0.0.1"
    logs = caplog.records
    assert logs[-1].levelno in [logging.INFO]
    assert logs[-1].msg == f"Built image {im_name} ({_id})"

    for log in logs:
        assert log.levelno in [logging.INFO, logging.WARNING]
