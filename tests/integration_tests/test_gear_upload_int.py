import argparse
import json
import os
from pathlib import Path

import flywheel
import pytest

from flywheel_gear_cli.gear.gear_upload import gear_upload


@pytest.mark.docker
@pytest.mark.skip(reason="require a local version of flywheel")
def test_gear_upload_full_integration(mocker, tmpdir, manifest):
    manifest["name"] = "alpine"
    manifest["version"] = "latest"
    manifest["custom"]["gear-builder"]["image"] = "alpine:latest"
    manifest_file = Path(tmpdir / "manifest.json")
    domain_mock = mocker.patch("flywheel_gear_cli.gear.gear_upload.get_domain")
    domain_mock.return_value = os.environ.get("DOCKER_REGISTRY", "localhost:5000")

    mocker.patch("docker.client.DockerClient.login")

    with open(str(manifest_file), "w") as fp:
        json.dump(manifest, fp)

    args = argparse.Namespace(
        dir=tmpdir, fw_keys=["localhost:8080:__force_insecure:admin-apikey"]
    )

    gear_upload(args)

    fw = flywheel.Client("localhost:8080:__force_insecure:admin-apikey")

    gears = fw.gears.find()
    assert len(gears) == 1
    assert gears[0].gear.name == "alpine"
    assert (
        gears[0].exchange.rootfs_url
        == f"docker://{os.getenv('DOCKER_REGISTRY','localhost:5000')}/alpine:latest"
    )
