import argparse
import json
import logging
import shlex
from pathlib import Path
from unittest.mock import MagicMock, patch

import flywheel
import pytest

from flywheel_gear_cli.job.job_pull import add_command, job_pull

log = logging.getLogger(__name__)


def create_args(**kwargs):
    """Create mocked parsed CLI args"""
    kwargs.setdefault("job_id", None)
    kwargs.setdefault("output_dir", None)
    kwargs.setdefault("add_key", False)
    kwargs.setdefault("mnt_cwd", False)

    return kwargs


def test_add_command():
    # Mock subparsers object and replace add_parser to return a regular ArgumentParser
    subparsers = MagicMock(**{"add_parser.return_value": argparse.ArgumentParser()})
    parser = add_command(subparsers, None)

    def assert_parsed_params(command, **kwargs):
        # Access parsed args as dict
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(**kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    with pytest.raises(SystemExit):
        parser.parse_args([])

    assert_parsed_params(
        "job-id /tmp",
        job_id="job-id",
        output_dir="/tmp",
        add_key=False,
        mnt_cwd=False,
    )
    assert_parsed_params(
        "job-id /tmp --add-key",
        job_id="job-id",
        output_dir="/tmp",
        add_key=True,
        mnt_cwd=False,
    )
    assert_parsed_params(
        "job-id /tmp --mnt-cwd",
        job_id="job-id",
        output_dir="/tmp",
        add_key=False,
        mnt_cwd=True,
    )
    assert_parsed_params(
        "job-id /tmp --add-key --mnt-cwd",
        job_id="job-id",
        output_dir="/tmp",
        add_key=True,
        mnt_cwd=True,
    )


def test_job_pull_raises_is_user_is_not_root():
    client = MagicMock()
    client.return_value.get_current_user.return_value = flywheel.User(
        email="toto@cucumber.isgood", root=False
    )
    args = argparse.Namespace(
        job_id="a-job-id", output_dir="", add_key=True, mnt_cwd=True
    )
    with patch("flywheel.Client", client):
        with pytest.raises(ValueError):
            job_pull(args)


def test_job_pull(mocker, tmpdir, user, gear_doc, job):
    load_auth_config = mocker.patch("flywheel_gear_cli.job.job_pull.load_auth_config")
    load_auth_config.return_value = {
        "key": "current-user-api-key",
        "root": True,
    }
    client = MagicMock()
    client.return_value.get_current_user.return_value = user(root=True)
    client.return_value.get_gear.return_value = gear_doc()
    client.return_value.get_job.return_value = job()

    args = argparse.Namespace(
        job_id="a-job-id", output_dir=tmpdir, add_key=True, mnt_cwd=True
    )

    with patch("flywheel.Client", client):

        job_pull(args)

        root_dir = f"{gear_doc().gear.name}-{gear_doc().gear.version}-{job().id}"
        assert (Path(tmpdir) / root_dir).exists()
        assert (Path(tmpdir) / root_dir / "input").exists()
        assert (Path(tmpdir) / root_dir / "output").exists()
        assert (Path(tmpdir) / root_dir / "config.json").exists()
        with open((Path(tmpdir) / root_dir / "config.json"), "r") as fp:
            config = json.load(fp)
        assert config["inputs"]["api-key"]["key"] == "current-user-api-key"

    # # Add a test for downloading analysis files as assets
    # with patch("flywheel.Client", client):
    #     caplog.set_level(logging.DEBUG)
    #     mocker("flywheel_gear_cli.job.flywheel.get_job.job.config.get", return_value=
    #         {
    #             "inputs": {
    #                 "fake_file": {
    #                     "base": {
    #                         "file": {
    #                             {
    #                                 "hierarchy": {"type": "analysis", "id": "U218"},
    #                                 "location": {"name": "does_not_matter"},
    #                             }
    #                         }
    #                     }
    #                 }
    #             }
    #         }
    #     )
    #     job_pull(args)
    #     assert "analysis" in caplog.text
