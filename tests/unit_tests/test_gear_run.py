# pylint: disable=protected-access
import argparse
import shlex
import shutil
from pathlib import Path
from unittest.mock import Mock

import pytest

from flywheel_gear_cli.gear import gear_run


def test_config_file_not_present(tmpdir):
    with pytest.raises(SystemExit):
        gear_run._validate_config(tmpdir)


def test_config_not_valid(tmpdir):
    config = Path(tmpdir) / "config.json"
    config.touch()

    with pytest.raises(SystemExit):
        gear_run._validate_config(tmpdir)


def test_returns_config(tmpdir, mocker):
    config = Path(tmpdir) / "config.json"
    config.touch()
    config_mock = mocker.patch("flywheel_gear_cli.gear.gear_run.Config")

    gear_run._validate_config(tmpdir)
    config_mock.assert_called_once_with(path=(Path(tmpdir) / "config.json"))


@pytest.fixture(scope="function")
def manifest(mocker):
    manifest_mock = mocker.patch("flywheel_gear_cli.gear.gear_run.Manifest")
    manifest_mock.return_value.get_docker_image_name_tag.return_value = (
        "flywheel/test:0.0.1"
    )
    return manifest_mock


@pytest.fixture(scope="function")
def config(mocker):
    config_mock = mocker.patch("flywheel_gear_cli.gear.gear_run.Config")
    return config_mock


@pytest.fixture
def setup_mocks(mocker):
    manifest = mocker.patch("flywheel_gear_cli.gear.gear_run.validate_manifest")
    config = mocker.patch("flywheel_gear_cli.gear.gear_run._validate_config")
    json = mocker.patch("flywheel_gear_cli.gear.gear_run.json")
    shutil = mocker.patch("flywheel_gear_cli.gear.gear_run.shutil")
    return (manifest, config, json, shutil)


@pytest.mark.parametrize(
    "target,exp",
    [(None, "/tmp/gear/test_0.0.1"), ("/tmp/gear/my_test", "/tmp/gear/my_test")],
)
def test_setup_target_path(setup_mocks, target, exp):
    (manifest, config, _, shutil_mock) = setup_mocks
    manifest.return_value.name = "test"
    manifest.return_value.version = "0.0.1"
    _, t_dir = gear_run.setup(Path.cwd(), target=target)
    assert t_dir == Path(exp)
    manifest.assert_called_once_with(Path.cwd() / "manifest.json")
    config.assert_called_once_with(Path.cwd())
    for gear_dir in ["input", "output", "work"]:
        assert (t_dir / gear_dir).exists()
    shutil_mock.stop()
    shutil.rmtree(str(t_dir))


@pytest.mark.parametrize("input_map", [True, False])
def test_setup_input_map(setup_mocks, tmp_path, input_map):
    (manifest, config, json, shutil_mock) = setup_mocks
    manifest.return_value.inputs = {
        "test1": {
            "base": "file",
        },
        "test2": {
            "base": "file",
        },
        "test3": {
            "base": "file",
        },
        "test4": {
            "base": "api-key",
        },
    }
    config.return_value.inputs = {
        "test1": {"base": "file", "location": {"path": tmp_path / "test1.txt"}},
        "test2": {"base": "file", "location": {"path": tmp_path / "test2.txt"}},
    }
    if input_map:
        i_map = {str(tmp_path / "test1.txt"): str(tmp_path / "test1.txt.local")}
    else:
        i_map = {}
    json.load.return_value = i_map
    _ = gear_run.setup("/tmp", tmp_path)
    s_calls = [tuple(call[0]) for call in shutil_mock.copy.call_args_list]
    if input_map:
        assert shutil_mock.copy.call_count == 3
        assert (
            str(tmp_path / "test1.txt.local"),
            str(tmp_path / "input/test1/test1.txt"),
        ) in s_calls
    assert (tmp_path / "input/test1").exists()
    assert (tmp_path / "input/test2").exists()
    assert (
        str(Path("/tmp").resolve() / "config.json"),
        str(tmp_path / "config.json"),
    ) in s_calls
    assert (
        str(Path("/tmp").resolve() / "manifest.json"),
        str(tmp_path / "manifest.json"),
    ) in s_calls


def test_gear_run_assets_exist(tmp_path, mocker):
    args = argparse.Namespace(
        dir=str(tmp_path), interactive=False, no_rm=True, mount_cwd=False
    )
    (tmp_path / "run.sh").touch()
    mocker.patch("flywheel_gear_cli.gear.gear_run.adjust_run_sh")
    os_patch = mocker.patch("os.chmod")
    sp_patch = mocker.patch("subprocess.check_output")
    gear_run.gear_run(args)
    os_patch.assert_called_once_with(str(tmp_path / "run.sh"), 0o0755)
    sp_patch.assert_called_once_with([str(tmp_path / "run.sh")])


def test_gear_run_assets_dont_exist(mocker):
    args = argparse.Namespace(dir="test")
    os_patch = mocker.patch("os.chmod")
    sp_patch = mocker.patch("subprocess.run")
    r = gear_run.gear_run(args)
    assert r == 1
    os_patch.assert_not_called()
    sp_patch.assert_not_called()


def test_gear_run_target_successful(mocker, tmp_path):
    args = argparse.Namespace(
        dir=Path.cwd(),
        target=Path(tmp_path),
        interactive=False,
        no_rm=True,
        mount_cwd=False,
    )
    s = mocker.patch.object(gear_run, "setup")
    s.return_value = ("a", tmp_path)
    os_patch = mocker.patch("os.chmod")
    sp_patch = mocker.patch("subprocess.check_output")
    b = gear_run.gear_run(args)
    os_patch.assert_called_once_with(str(tmp_path / "run.sh"), 0o0755)
    sp_patch.assert_called_once_with([str(tmp_path / "run.sh")])
    assert b == 0


def test_gear_run_target_unsuccessful(mocker, tmp_path):
    args = argparse.Namespace(
        dir=Path(tmp_path),
        target=Path(tmp_path),
        interactive=False,
        no_rm=False,
    )
    s = mocker.patch.object(gear_run, "setup")
    mocker.patch("os.chmod")
    mocker.patch("subprocess.check_output")
    s.side_effect = ValueError()
    b = gear_run.gear_run(args)
    assert b == 1


###################### cli tests
def test_add_command():
    # Mock subparsers object and replace add_parser to return a regular ArgumentParser
    subparsers = Mock(**{"add_parser.return_value": argparse.ArgumentParser()})
    parser = gear_run.add_command(subparsers, None)

    def create_args(**kwargs):
        """Create mocked parsed CLI args"""
        kwargs.setdefault("dir", None)
        kwargs.setdefault("no_rm", False)
        kwargs.setdefault("interactive", False)
        kwargs.setdefault("volumes", None)
        kwargs.setdefault("target", None)
        return kwargs

    def assert_parsed_params(command, **kwargs):
        # Access parsed args as dict
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(**kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    assert_parsed_params(
        "/tmp --no-rm --interactive",
        dir="/tmp",
        target=None,
        no_rm=True,
        interactive=True,
        volumes=None,
    )
    assert_parsed_params(
        "--target ~/test --no-rm /tmp",
        dir="/tmp",
        target="~/test",
        no_rm=True,
        interactive=False,
        volumes=None,
    )
    assert_parsed_params(
        "--target ~/test --no-rm /tmp --volumes '/tmp/gear:/flywheel/v0' '/test:/test'",
        dir="/tmp",
        target="~/test",
        no_rm=True,
        interactive=False,
        volumes=["/tmp/gear:/flywheel/v0", "/test:/test"],
    )
