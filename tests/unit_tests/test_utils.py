import json
import time
from pathlib import Path

import pytest

from flywheel_gear_cli.utils import (
    get_credentials,
    handle_docker_build,
    handle_docker_push,
)


@pytest.mark.parametrize(
    "api_key, expected",
    [
        [
            "localhost:1234",
            {
                "host": "localhost",
                "unique": "1234",
                "api_key": "localhost:1234",
                "port": None,
                "opt": None,
            },
        ],
        [
            "localhost:__force_insecure:asdf3",
            {
                "host": "localhost",
                "unique": "asdf3",
                "api_key": "localhost:asdf3",
                "port": None,
                "opt": "__force_insecure",
            },
        ],
        [
            "localhost:443:__force_insecure:1234",
            {
                "host": "localhost",
                "unique": "1234",
                "api_key": "localhost:1234",
                "port": "443",
                "opt": "__force_insecure",
            },
        ],
    ],
)
def test_get_credentials(api_key, expected):

    out = get_credentials(api_key)
    assert out == expected


def mock_push_response():
    example_file = Path(__file__).parents[1] / "data/example_docker_push_out.txt"
    with open(example_file, "r") as fp:
        for line in fp:
            time.sleep(0.05)
            yield json.loads(line)


def mock_build():
    example_file = Path(__file__).parents[1] / "data/docker_build_out.txt"
    with open(example_file, "r") as fp:
        for line in fp:
            time.sleep(0.05)
            yield json.loads(line)


# Only for visual output, run with pytest -s to see
@pytest.mark.skip(reason="Only for visual output, run with pytest -s to see")
def test_handle_docker_push_renders():
    out = handle_docker_push(mock_push_response())
    assert (
        out == "sha256:1e7b3b2918d55b0a7b6088125703788b310c4056b0fffddb87ce95820c701b34"
    )


# Only for visual output, run with pytest -s to see
@pytest.mark.skip(reason="Only for visual output, run with pytest -s to see")
def test_handle_docker_build_renders():
    out = handle_docker_build(mock_build())
    assert (
        out == "sha256:15d93703937c4252ecd07044402b925a0bee7e3736bb8d8f3c504a5cb1541273"
    )
