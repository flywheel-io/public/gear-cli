# pylint: disable=too-many-arguments
import argparse
import json
import logging
import shlex
import shutil
from pathlib import Path
from unittest.mock import Mock

import pytest

from flywheel_gear_cli.gear.gear_config import (
    _wrap_arg_name,
    add_command,
    gear_config,
    get_args,
    handle_arg,
    populate_input_map,
    save_backup,
)


@pytest.fixture(scope="session", autouse=True)
def assets():
    return (Path(__file__).parents[1] / "data").resolve()


@pytest.fixture(scope="session")
def i_c_dict():
    return {
        "i_dict": {
            "dcm2niix_input": "dcm2niix_input",
            "rec_file_input": "rec_file_input",
            "pydeface_template": "pydeface_template",
            "pydeface_facemask": "pydeface_facemask",
            "debug_config": "debug",
        },
        "c_dict": {
            "anonymize_bids": {"name": "anonymize_bids", "default": True},
            "bids_sidecar": {"name": "bids_sidecar", "default": "n"},
            "coil_combine": {"name": "coil_combine", "default": False},
            "comment": {"name": "comment", "default": ""},
            "timezone_config": {"name": "timezone"},  # handle rename case
        },
    }


@pytest.mark.parametrize("mock", [True, False])
@pytest.mark.parametrize("exists", [True, False])
def test_create(tmp_path, mocker, exists, caplog, mock, assets):
    caplog.set_level(logging.INFO)
    if mock:
        conf = mocker.patch("flywheel_gear_cli.gear.gear_config.Config")
        man = mocker.patch("flywheel_gear_cli.gear.gear_config.Manifest")
    else:
        shutil.copy(
            str(assets / "manifests/dcm2niix.json"),
            str(Path(tmp_path) / "manifest.json"),
        )

    args = argparse.Namespace(pwd=Path(tmp_path), create=True)

    if exists:
        (Path(tmp_path) / "config.json").touch()

    gear_config(args)

    if mock:
        conf.default_config_from_manifest.assert_called_once_with(man.return_value)
        conf.default_config_from_manifest.return_value.to_json.assert_called_once()
    else:
        out = Path(tmp_path) / "config.json"
        out_back = Path(tmp_path) / "config.json.back"
        assert out.exists() and out.is_file()
        if exists:
            assert out_back.exists() and out_back.is_file()
        with open(out, "r") as o_file:
            a = json.load(o_file)
            print(a)

    if exists:
        assert len(caplog.record_tuples) == 2
        assert "saved" in caplog.record_tuples[0][2]
    else:
        assert len(caplog.record_tuples) == 1
        assert "Config" in caplog.record_tuples[0][2]


def test_update_config_manifest_doesnt_exist(mocker, tmp_path, caplog):
    caplog.set_level(logging.INFO)
    mocker.patch("flywheel_gear_cli.gear.gear_config.Config")
    args = argparse.Namespace(pwd=Path(tmp_path), create=True)
    out = gear_config(args)
    assert out
    assert len(caplog.record_tuples) == 2
    assert caplog.record_tuples[1][1] == logging.ERROR


def test_update_config_doesnt_exist(tmp_path, caplog, mocker):
    caplog.set_level(logging.INFO)
    args = argparse.Namespace(pwd=Path(tmp_path), create=False)
    mocker.patch("flywheel_gear_cli.gear.gear_config.Manifest")
    out = gear_config(args)
    assert out
    assert len(caplog.record_tuples) == 3
    assert caplog.record_tuples[1][1] == logging.ERROR


@pytest.mark.parametrize(
    "to_add,exp",
    [
        ({"comment": "test"}, {"comment": "test"}),
        ({"timezone_config": "test_tz"}, {"timezone": "test_tz"}),
    ],
)
def test_update_config(tmp_path, assets, to_add, exp, i_c_dict):
    shutil.copy(
        str(assets / "manifests/dcm2niix.json"),
        str(Path(tmp_path) / "manifest.json"),
    )
    shutil.copy(
        str(assets / "manifests/dcm2niix_default_config.json"),
        str(Path(tmp_path) / "config.json"),
    )
    args = argparse.Namespace(
        pwd=Path(tmp_path),
        create=False,
        input_dict=i_c_dict["i_dict"],
        conf_dict=i_c_dict["c_dict"],
        **to_add,
    )

    gear_config(args)

    with open(str(Path(tmp_path) / "config.json"), "r") as o_file:
        d = json.load(o_file)
        for k, v in exp.items():
            assert k in d["config"]
            assert d["config"][k] == v


@pytest.mark.parametrize("exists", [True, False])
@pytest.mark.parametrize(
    "to_add,exp",
    [
        ({"dcm2niix_input": "test.txt"}, {"dcm2niix_input": "test.txt"}),
        ({"debug_config": "test_tz"}, {"debug": "test_tz"}),
    ],
)
def test_update_inputs(tmp_path, caplog, assets, to_add, exp, i_c_dict, exists):
    if exists:
        to_add = {k: str(Path(tmp_path) / v) for k, v in to_add.items()}
        exp = {k: str(Path(tmp_path) / v) for k, v in exp.items()}
    caplog.set_level(logging.INFO)
    shutil.copy(
        str(assets / "manifests/dcm2niix.json"),
        str(Path(tmp_path) / "manifest.json"),
    )
    shutil.copy(
        str(assets / "manifests/dcm2niix_default_config.json"),
        str(Path(tmp_path) / "config.json"),
    )
    for v in to_add.values():
        (Path(tmp_path) / v).touch()

    args = argparse.Namespace(
        pwd=Path(tmp_path),
        create=False,
        input_dict=i_c_dict["i_dict"],
        conf_dict=i_c_dict["c_dict"],
        **to_add,
    )

    gear_config(args)

    if exists:
        with open(str(Path(tmp_path) / "config.json"), "r") as o_file:
            d = json.load(o_file)
            for k, v in exp.items():
                assert k in d["inputs"]
                a = d["inputs"][k]
                assert a["base"] == "file"
                assert a["location"]["name"] == k
                assert a["location"]["path"].startswith(f"/flywheel/v0/input/{k}")
    else:
        assert caplog.record_tuples[-1][1] == logging.ERROR


def test_update_input_api_key(caplog, tmp_path, assets, i_c_dict):
    caplog.set_level(logging.INFO)
    shutil.copy(
        str(assets / "manifests/dcm2niix.json"),
        str(Path(tmp_path) / "manifest.json"),
    )
    shutil.copy(
        str(assets / "manifests/dcm2niix_default_config.json"),
        str(Path(tmp_path) / "config.json"),
    )
    args = argparse.Namespace(
        pwd=Path(tmp_path),
        create=False,
        input_dict=i_c_dict["i_dict"],
        conf_dict=i_c_dict["c_dict"],
        api_key="test_key",
    )
    gear_config(args)

    with open(str(Path(tmp_path) / "config.json"), "r") as o_file:
        d = json.load(o_file)
        assert "api-key" in d["inputs"]
        a = d["inputs"]["api-key"]
        assert a["base"] == "api-key"
        assert a["key"] == "test_key"


def test_update_w_no_conf_i_dict(caplog, assets, tmp_path, mocker):
    conf_patch = mocker.patch("flywheel_gear_cli.gear.gear_config.Config")
    caplog.set_level(logging.INFO)
    shutil.copy(
        str(assets / "manifests/dcm2niix.json"),
        str(Path(tmp_path) / "manifest.json"),
    )
    shutil.copy(
        str(assets / "manifests/dcm2niix_default_config.json"),
        str(Path(tmp_path) / "config.json"),
    )
    args = argparse.Namespace(
        pwd=Path(tmp_path),
        create=False,
    )
    out = gear_config(args)
    conf_patch.return_value.update_config.assert_called_once_with({})
    conf_patch.return_value.to_json.assert_called_once()
    assert caplog.record_tuples[-1][2] == (
        "Could not find config and/or inputs.  " "Nothing will be updated."
    )
    assert out == 0


@pytest.mark.parametrize("exists", [False, True])
def test_populate_input_map(tmp_path, exists, mocker):
    path = mocker.patch("flywheel_gear_cli.gear.gear_config.Path")
    path.return_value.expanduser.return_value = tmp_path / "test.json"
    if exists:
        with open(tmp_path / "test.json", "w") as fp:
            json.dump({"test1": "test2"}, fp)

    populate_input_map("test3", "test4")

    with open(tmp_path / "test.json", "r") as fp:
        input_map = json.load(fp)
        if exists:
            assert input_map == {"test1": "test2", "test4": "test3"}
        else:
            assert input_map == {"test4": "test3"}


@pytest.mark.parametrize("exists", [True, False])
def test_save_backup(tmp_path, mocker, exists):
    test_dir = Path(tmp_path).resolve()
    cwd = mocker.patch.object(Path, "cwd")
    cwd.return_value = test_dir

    conf = test_dir / "config.json"
    if exists:
        conf.touch()

    ret = save_backup(conf)

    if exists:
        out = Path(test_dir) / "config.json.back"
        assert ret == out
        assert out.exists() and out.is_file()
    else:
        assert ret is None


def test_get_args_dcm_niix(assets, i_c_dict):
    parser = argparse.ArgumentParser(prog="test test gear config")
    parser.add_argument("--timezone")
    parser.add_argument("--debug")

    manifest_path = assets / "manifests/dcm2niix.json"

    c_dict, i_dict = get_args(parser, manifest_path)

    i_dict_exp = i_c_dict["i_dict"]

    c_dict_exp = i_c_dict["c_dict"]

    assert c_dict == c_dict_exp
    assert i_dict == i_dict_exp


@pytest.mark.parametrize(
    "name,val,exp",
    [
        (
            "cov",
            {"description": "test", "type": "string", "default": "all"},
            ("cov", "cov", "store", "test"),
        ),
        (
            "test-option",
            {"description": "test", "type": "string", "default": "test"},
            ("test-option", "test_option", "store", "test"),
        ),
    ],
)
def test_handle_arg(name, val, exp):
    assert handle_arg(name, val) == exp


@pytest.mark.parametrize(
    "name,exp", [("test", ("test", "test")), ("test_1", ("test-1", "test_1"))]
)
def test_wrap_args(name, exp):
    assert _wrap_arg_name(name) == exp


############ cli tests
def create_args(**kwargs):
    """Create mocked parsed CLI args"""
    kwargs.setdefault("create", False)

    return kwargs


def test_add_command():
    # Mock subparsers object and replace add_parser to return a regular ArgumentParser
    subparsers = Mock(**{"add_parser.return_value": argparse.ArgumentParser()})

    parser = add_command(subparsers, None)

    def assert_parsed_params(command, **kwargs):
        # Access parsed args as dict
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(**kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    assert_parsed_params("--create", create=True)
    assert_parsed_params("", create=False)
