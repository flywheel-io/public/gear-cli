import argparse
import shlex
from argparse import Namespace
from pathlib import Path
from unittest.mock import Mock

import pytest

import flywheel_gear_cli.utils
from flywheel_gear_cli.gear import gear_validate_manifest


def test_manifest_file_not_present(tmpdir):
    with pytest.raises(SystemExit):
        flywheel_gear_cli.utils.validate_manifest(Path(tmpdir))


def test_manifest_not_valid(tmpdir):
    manifest = Path(tmpdir) / "manifest.json"
    manifest.touch()
    with pytest.raises(SystemExit):
        flywheel_gear_cli.utils.validate_manifest(manifest)


def test_returns_manifest(tmpdir, mocker):
    manifest = Path(tmpdir).resolve() / "manifest.json"
    manifest.touch()
    manifest_mock = mocker.patch("flywheel_gear_cli.utils.Manifest")

    flywheel_gear_cli.utils.validate_manifest(manifest)

    manifest_mock.assert_called_once_with(manifest=manifest)


def test_validate_manifest_wrapper_no_args(tmpdir, mocker):
    manifest = Path(tmpdir).resolve() / "manifest.json"
    manifest.touch()
    args = Namespace(path=manifest)

    manifest_mock = mocker.patch("flywheel_gear_cli.utils.Manifest")
    gear_validate_manifest.validate_manifest_wrapper(args)

    manifest_mock.assert_called_once_with(manifest=manifest)


def test_add_command():
    # Mock subparsers object and replace add_parser to return a regular ArgumentParser
    subparsers = Mock(**{"add_parser.return_value": argparse.ArgumentParser()})
    parser = gear_validate_manifest.add_command(subparsers, None)

    def create_args(**kwargs):
        """Create mocked parsed CLI args"""
        kwargs.setdefault("path", None)
        return kwargs

    def assert_parsed_params(command, **kwargs):
        # Access parsed args as dict
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(**kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    assert_parsed_params(
        "",
        path=None,
    )
    assert_parsed_params(
        "path/to/manifest.json",
        path="path/to/manifest.json",
    )
