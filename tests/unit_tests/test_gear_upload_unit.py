import argparse
import logging
import shlex
from unittest.mock import Mock

import flywheel
import pytest

from flywheel_gear_cli.gear.gear_upload import add_command, add_gear, gear_upload, login


@pytest.fixture
def mock_gear_upload_locals(mocker):
    manifest = mocker.patch("flywheel_gear_cli.gear.gear_upload.Manifest")
    get_domain = mocker.patch("flywheel_gear_cli.gear.gear_upload.get_domain")
    login = mocker.patch("flywheel_gear_cli.gear.gear_upload.login")
    add_gear = mocker.patch("flywheel_gear_cli.gear.gear_upload.add_gear")
    sdk_client = mocker.patch("flywheel_gear_cli.gear.gear_upload.get_sdk_client")
    path = mocker.patch("flywheel_gear_cli.gear.gear_upload.Path")
    open_mock = mocker.patch("builtins.open")
    json_mock = mocker.patch("json.load")

    return (
        manifest,
        get_domain,
        login,
        add_gear,
        path,
        open_mock,
        json_mock,
        sdk_client,
    )


@pytest.mark.parametrize(
    "args,calls",
    [(argparse.Namespace(keys=["test"]), 0), (argparse.Namespace(), 1)],
)
def test_gear_upload_key(mock_gear_upload_locals, args, calls):
    path = mock_gear_upload_locals[4]
    path.return_value.expanduser.return_value = ""
    json_mock = mock_gear_upload_locals[6]
    json_mock.return_value = {"key": ["test"]}

    gear_upload(args)

    assert path.return_value.expanduser.call_count == calls


@pytest.mark.parametrize(
    "args,calls",
    [(argparse.Namespace(dir="test"), 0), (argparse.Namespace(), 1)],
)
def test_gear_upload_manifest(mock_gear_upload_locals, args, calls):
    path = mock_gear_upload_locals[4]
    gear_upload(args)
    assert path.cwd.call_count == calls


def test_gear_upload_calls(mock_gear_upload_locals):
    args = argparse.Namespace(keys=["test1", "test2"])
    gear_upload(args)

    get_domain = mock_gear_upload_locals[1]
    get_sdk = mock_gear_upload_locals[7]
    get_user = get_sdk.return_value.get_current_user
    login = mock_gear_upload_locals[2]
    handle_image = mock_gear_upload_locals[3]
    mocks = [get_domain, get_sdk, get_user, login, handle_image]

    assert all(mock.call_count == 2 for mock in mocks)


@pytest.mark.parametrize(
    "api_key, out_key",
    [
        ("localhost:8080:admin", "localhost:admin"),
        ("localhost:8080:__force_insecure:admin", "localhost:admin"),
        ("localhost:admin", "localhost:admin"),
    ],
)
def test_login(api_key, out_key, get_docker_client, user):
    user = user()
    _ = login(api_key, user, "localhost")
    get_docker_client.assert_called_once()
    get_docker_client.return_value.login.assert_called_once_with(
        username=user.email, password=out_key, registry="https://localhost/v2"
    )


def test_add_gear(manifest, docker_client, sdk_mock, mocker):
    sdk_mock.prepare_add_gear.return_value = "test_ticket"
    handle_push = mocker.patch("flywheel_gear_cli.gear.gear_upload.handle_docker_push")
    handle_push.return_value = "test_digest"
    gear_doc = {}
    gear_doc["gear"] = manifest
    src_image = manifest["custom"]["gear-builder"]["image"]  # TODO

    add_gear(docker_client, src_image, gear_doc, sdk_mock, "localhost")

    docker_client.images.get.assert_called_once_with(src_image)
    docker_client.images.get.return_value.tag.assert_called_once_with(
        "localhost/test", "0.0.1"
    )

    docker_client.images.push.assert_called_once_with(
        "localhost/test", tag="0.0.1", stream=True, decode=True
    )
    gear_doc.update({"exchange": {"rootfs-url": "docker://localhost/test:0.0.1"}})
    sdk_mock.prepare_add_gear.assert_called_once_with(gear=gear_doc)
    sdk_mock.save_gear.assert_called_once_with(
        {"ticket": "test_ticket", "repo": "test", "pointer": "test_digest"}
    )


@pytest.mark.parametrize(
    "status,log_entry",
    [(409, "Gear already exists"), (410, "Error occured uploading gear")],
)
def test_add_gear_api_except(
    manifest, docker_client, sdk_mock, caplog, status, log_entry
):
    caplog.set_level(logging.INFO)
    gear_doc = {}
    gear_doc["gear"] = manifest
    src_image = manifest["custom"]["gear-builder"]["image"]  # TODO
    # TODO: check if its prepare_add or save_gear that throws a 409
    sdk_mock.save_gear.side_effect = flywheel.rest.ApiException(status=status)
    docker_client.images.push.return_value = [{"status": "test", "id": "1"}]

    add_gear(docker_client, src_image, gear_doc, sdk_mock, "localhost")
    assert caplog.record_tuples[-1][2] == log_entry


############ cli tests
def create_args(**kwargs):
    """Create mocked parsed CLI args"""
    kwargs.setdefault("dir", None)
    kwargs.setdefault("keys", [])

    return kwargs


def test_add_command():
    # Mock subparsers object and replace add_parser to return a regular ArgumentParser
    subparsers = Mock(**{"add_parser.return_value": argparse.ArgumentParser()})
    parser = add_command(subparsers, None)

    def assert_parsed_params(command, **kwargs):
        # Access parsed args as dict
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(**kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    # Exit if no args passed
    # with pytest.raises(SystemExit):
    #    parser.parse_args([])

    assert_parsed_params("/tmp", dir="/tmp", keys=None)
    assert_parsed_params(
        "/tmp --keys localhost:8080 ss.ce.flywheel.io:admin",
        dir="/tmp",
        keys=["localhost:8080", "ss.ce.flywheel.io:admin"],
    )
