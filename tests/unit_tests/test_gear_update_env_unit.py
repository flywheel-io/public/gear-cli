import argparse
import logging
import shlex
from pathlib import Path
from unittest.mock import MagicMock, Mock

import pytest

from flywheel_gear_cli.gear.gear_update_env import (
    add_command,
    handle_env,
    validate_args,
)


# Validate args
# * build blocklist
# * exit if directory, manifest don't exist or if manifest invalid
# *
@pytest.mark.parametrize("files", [[], ["manifest.json"]])
def test_validate_args_invalid_manifest(tmpdir, files, caplog):
    temp_dir = Path(tmpdir).resolve()
    for file in files:
        Path(temp_dir / file).touch()
    args = argparse.Namespace(dir=str(temp_dir))
    with pytest.raises(SystemExit):
        _ = validate_args(args)

        msgs = caplog.get_records()
        assert len(msgs) == 1
        assert msgs[0].level == logging.ERROR


@pytest.mark.parametrize(
    "blocklist_in,blocklist_exp",
    [
        [[], {"TERM", "HOSTNAME", "HOME"}],
        [["TERM=no"], {"HOSTNAME", "HOME"}],
        [
            ["TERM=no", "HOSTNAME=yes", "LSCOLORS"],
            {"LSCOLORS", "HOSTNAME", "HOME"},
        ],
    ],
)
def test_validate_args_blocklist_valid(mocker, tmpdir, blocklist_in, blocklist_exp):
    args = argparse.Namespace(blocklist=blocklist_in)
    is_dir = mocker.patch("pathlib.Path.is_dir")
    load = mocker.patch("flywheel_gear_cli.gear.gear_update_env.Manifest")
    # Touch temporary files
    _ = Path(tmpdir)
    args.dir = tmpdir

    # Run validate args
    _, _, blocklist, _ = validate_args(args)

    # Asserts
    is_dir.assert_called_once()
    load.assert_called_once()

    assert blocklist == blocklist_exp


# Handle Env
@pytest.mark.parametrize(
    "env_gen",
    [
        [
            b"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\n",
            b"HOSTNAME=f012a8b25fb5\n",
            b"HOME=/root\n",
        ]
    ],
)
def test_handle_env(mocker, tmpdir, env_gen, caplog):
    manifest_mock = MagicMock()
    #    manifest_mock['environment'] = {"TEST01": "ABCD"}
    json_patch2 = mocker.patch("json.dumps")

    def dry_run():
        handle_env(env_gen, manifest_mock, Path(tmpdir / "test.txt"), {}, True)

        json_patch2.assert_called_once()
        manifest_mock.environment.update.assert_called_once()
        manifest_mock.to_json.assert_not_called()

        for message in caplog.get_records(when="call"):
            assert message.levelno in [logging.INFO, logging.DEBUG]

    def wet_run():
        handle_env(env_gen, manifest_mock, Path(tmpdir / "test.txt"), {}, False)

        manifest_mock.environment.update.assert_called_once()
        manifest_mock.to_json.assert_called_once()
        json_patch2.assert_called_once()

    dry_run()
    manifest_mock.reset_mock()
    json_patch2.reset_mock()
    wet_run()


############ cli tests
def create_args(**kwargs):
    """Create mocked parsed CLI args"""
    kwargs.setdefault("dir", None)
    kwargs.setdefault("dry_run", False)
    kwargs.setdefault("blocklist", None)

    return kwargs


def test_add_command():
    # Mock subparsers object and replace add_parser to return a regular ArgumentParser
    subparsers = Mock(**{"add_parser.return_value": argparse.ArgumentParser()})
    parser = add_command(subparsers, None)

    def assert_parsed_params(command, **kwargs):
        # Access parsed args as dict
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(**kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    # Exit if no args passed
    # with pytest.raises(SystemExit):
    #    parser.parse_args([])

    assert_parsed_params("/tmp", dir="/tmp", dry_run=False, blocklist=None)
    assert_parsed_params("/tmp --dry-run", dir="/tmp", dry_run=True, blocklist=None)
    assert_parsed_params(
        "/tmp --dry-run --blocklist", dir="/tmp", dry_run=True, blocklist=[]
    )
    assert_parsed_params(
        "/tmp --dry-run --blocklist name=yes",
        dir="/tmp",
        dry_run=True,
        blocklist=["name=yes"],
    )
    assert_parsed_params(
        "/tmp --dry-run --blocklist name=yes obj1=no",
        dir="/tmp",
        dry_run=True,
        blocklist=["name=yes", "obj1=no"],
    )
