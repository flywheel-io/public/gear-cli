import argparse
import logging
import shlex
from pathlib import Path
from unittest.mock import MagicMock, Mock

import pytest

from flywheel_gear_cli.gear.gear_build import (
    add_command,
    gear_build,
    handle_docker,
    validate_args,
)


############ validate_args tests
def test_validate_args_dir_not_exist(caplog):
    args = argparse.Namespace()
    with pytest.raises(SystemExit):
        _ = validate_args(args)

        msgs = caplog.get_records()
        assert len(msgs) == 1
        assert msgs[0].level == logging.ERROR


@pytest.mark.parametrize(
    "files",
    [
        ["Dockerfile"],
        ["Dockerfile", "manifest.json"],
        ["run.py", "manifest.json"],
        ["Dockerfile", "run.py"],
    ],
)
def test_validate_args_files_not_present(files, tmpdir, caplog):
    tmp_dir = Path(tmpdir)
    for file in files:
        Path(tmp_dir / file).touch()

    args = argparse.Namespace(dir=str(tmp_dir.resolve()))
    with pytest.raises(SystemExit):
        _ = validate_args(args)

        msgs = caplog.get_records()
        assert len(msgs) == 1
        assert msgs[0].level == logging.ERROR


def test_validate_args_file_exists(mocker, tmpdir):
    is_dir = mocker.patch("pathlib.Path.is_dir")
    exists = mocker.patch("pathlib.Path.exists")
    load = mocker.patch("flywheel_gear_cli.gear.gear_build.Manifest")
    load.return_value.get_docker_image_name_tag.return_value = "test:0.1.1"
    # Touch temporary files
    docker = Path(tmpdir / "Dockerfile")
    manifest = Path(tmpdir / "manifest.json")
    run = Path(tmpdir / "run.py")

    docker.touch()
    manifest.touch()
    run.touch()

    # Build args
    args = argparse.Namespace(dir=str(tmpdir))

    # Run validate args
    image_tag, mount_dir, build_args = validate_args(args)

    # Asserts
    is_dir.assert_called_once()
    assert exists.call_count == 2
    load.assert_called()
    assert str(mount_dir) == str(tmpdir)
    assert build_args == {}
    assert image_tag == "test:0.1.1"


@pytest.mark.parametrize(
    "my_build_args,expected",
    [
        (
            ["image=test:0.1", "other_arg=test"],
            {"image": "test:0.1", "other_arg": "test"},
        ),
        ([], {}),
    ],
)
def test_validate_args_build_args(mocker, my_build_args, expected):
    mocker.patch("pathlib.Path.is_dir")
    mocker.patch("pathlib.Path.exists")
    mocker.patch("json.load")
    mocker.patch("builtins.open")
    load_patch = mocker.patch("flywheel_gear_cli.gear.gear_build.Manifest")
    load_patch.return_value.get_docker_image_name_tag.return_value = "test:0.1.1"

    args = argparse.Namespace(build_args=my_build_args)

    image_tag, mount_dir, build_args = validate_args(args)

    assert expected == build_args
    assert image_tag == "test:0.1.1"
    assert mount_dir == Path.cwd()


def test_validate_args_mount_dir_not_dir(tmp_path, caplog):
    caplog.set_level(logging.ERROR)
    args = MagicMock()
    args.dir = tmp_path / "test.txt"

    with pytest.raises(SystemExit):
        validate_args(args)
    assert len(caplog.record_tuples) == 1


############ gear_build tests


def test_gear_build(mocker):
    val = mocker.patch("flywheel_gear_cli.gear.gear_build.validate_args")
    val.return_value = ("test", "test_dir", "--test")
    hd = mocker.patch("flywheel_gear_cli.gear.gear_build.handle_docker")
    args = MagicMock()
    assert gear_build(args) == 0
    val.assert_called_once_with(args)
    hd.assert_called_once_with(
        "test", "test_dir", "--test", **{"pull": False, "rm": False}
    )


############ handle_docker tests


@pytest.mark.parametrize(
    "image_tag, build_args, pull, rm",
    [
        [
            "test:0.0.1",
            {"image": "test:0.1", "other_arg": "test"},
            True,
            True,
        ],
        [
            "test:0.0.1",
            {"image": "test:0.1", "other_arg": "test"},
            True,
            True,
        ],
    ],
)
def test_handle_docker(  # pylint: disable=too-many-arguments
    mocker, tmpdir, image_tag, build_args, pull, rm
):
    docker_mock = mocker.patch("docker.APIClient")
    build_mock = mocker.patch("flywheel_gear_cli.gear.gear_build.handle_docker_build")
    build_mock.return_value = "test"

    im_id = handle_docker(image_tag, Path(tmpdir), build_args, pull, rm)

    docker_mock.return_value.build.assert_called_once_with(
        path=str(Path(tmpdir).resolve()),
        tag=image_tag,
        rm=rm,
        pull=pull,
        buildargs=build_args,
        decode=True,
    )
    assert im_id == "test"


############ cli tests
def create_args(**kwargs):
    """Create mocked parsed CLI args"""
    kwargs.setdefault("dir", None)
    kwargs.setdefault("no_rm", False)
    kwargs.setdefault("no_pull", False)
    kwargs.setdefault("build_args", None)

    return kwargs


def test_add_command():
    # Mock subparsers object and replace add_parser to return a regular ArgumentParser
    subparsers = Mock(**{"add_parser.return_value": argparse.ArgumentParser()})
    parser = add_command(subparsers, None)

    def assert_parsed_params(command, **kwargs):
        # Access parsed args as dict
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(**kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    # Exit if no args passed
    # with pytest.raises(SystemExit):
    #    parser.parse_args([])

    assert_parsed_params(
        "/tmp", dir="/tmp", no_rm=False, no_pull=False, build_args=None
    )
    assert_parsed_params(
        "/tmp --no-rm", dir="/tmp", no_rm=True, no_pull=False, build_args=None
    )
    assert_parsed_params(
        "/tmp --no-rm --no-pull",
        dir="/tmp",
        no_rm=True,
        no_pull=True,
        build_args=None,
    )
    assert_parsed_params(
        "/tmp --no-rm --no-pull --build-args",
        dir="/tmp",
        no_rm=True,
        no_pull=True,
        build_args=[],
    )
    assert_parsed_params(
        "/tmp --no-rm --no-pull --build-args image=test:0.1",
        dir="/tmp",
        no_rm=True,
        no_pull=True,
        build_args=["image=test:0.1"],
    )
    assert_parsed_params(
        "/tmp --no-rm --no-pull --build-args image=test:0.1 other_arg=test",
        dir="/tmp",
        no_rm=True,
        no_pull=True,
        build_args=["image=test:0.1", "other_arg=test"],
    )
