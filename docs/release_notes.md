# Release Notes

## 0.1.0-alpha.6

__Maintentance__:

* Temporarily remove `gear upload` while waiting for new version of SDK

## 0.1.0-alpha.2-5

* __NOT RELEASED__

## 0.1.0-alpha.1

Initial release:

__Enhancements__:

* Add `gear build`, `gear config`, `gear update-env`, `gear run`,
`gear update-env`, `job pull`
